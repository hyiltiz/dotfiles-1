# Setup fzf
# ---------
if [[ ! "$PATH" == */Users/hormetyiltiz/.fzf/bin* ]]; then
  export PATH="$PATH:/Users/hormetyiltiz/.fzf/bin"
fi

# Man path
# --------
if [[ ! "$MANPATH" == */Users/hormetyiltiz/.fzf/man* && -d "/Users/hormetyiltiz/.fzf/man" ]]; then
  export MANPATH="$MANPATH:/Users/hormetyiltiz/.fzf/man"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/Users/hormetyiltiz/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/Users/hormetyiltiz/.fzf/shell/key-bindings.bash"

