# modified commands
alias diff='diff'              # requires colordiff package
alias more='less'
alias df='df -h'
alias du='du -c -h'
alias mkdir='mkdir -p -v'
alias nano='nano -w'
alias ping='ping -n 5'
alias ..='cd ..'

# new commands
alias da='date "+%A, %B %d, %Y [%T]"'
alias du1='du --max-depth=1'
alias hist='history | grep'      # requires an argument
alias openports='netstat --all --numeric --programs --inet --inet6'
alias pg='ps -Af | grep $1'         # requires an argument (note: /usr/bin/pg is installed by the util-linux package; maybe a different alias name should be used)
alias psgrep='ps aux|grep '

# privileged access
alias sudo='sudo '
alias scat='sudo cat'
alias svim='sudo vim'
alias root='sudo su'
alias reboot='sudo reboot'
alias halt='sudo halt'
alias update='sudo pacman -Su'
alias netcfg='sudo netcfg2'

# ls
alias ls='ls -hF --color=auto'
alias lr='ls -R'                    # recursive ls
alias ll='ls -l'
alias la='ll -A'
alias lx='ll -BX'                   # sort by extension
alias lz='ll -rS'                   # sort by size
alias lt='ll -rt'                   # sort by date
alias lm='la | more'

# safety features
alias cp='cp -i'
alias mv='mv -i'
#alias rm='rm -i'                    # 'rm -i' prompts for every file (or use safe-rm)
alias ln='ln -i'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# dpkg deb control
alias inst='sudo apt-get install '
alias updt='sudo apt-get update '
alias upgd='sudo apt-get upgrade'
alias purge='sudo apt-get purge '
alias debr='dpkg-query -S ' # search installed files mathing pattern
alias debls='dpkg-query -L ' # list installed files given package
alias debs='apt-cache search ' # search 
alias debI='apt-cache show ' # info
alias depends='apt-cache depends '
alias rdepends='apt-cache rdepends '


# other useful utilities
alias tree='tree -ChAF'
alias du='du -h'
alias df='df -h'

alias gs='git status'
alias go='git checkout'
alias gb='git branch'
alias gc='git commit -a -m '
alias ga='git add '

alias octave='octave-cli --no-gui --silent --interactive '
alias qsync='rsync -atuz --info=progress2 '
alias 275print='lpr -P 275hp601  -o sides=two-sided-long-edge '
alias matShowLineNum='awk -F/ ''{print $1 "\ndisp("NR");"}''  '

alias igrep='grep -і '
