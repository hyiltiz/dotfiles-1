function fish_user_key_bindings -d "User defined key bindings"
bind \eg "commandline -a '|grep '"
bind \e. "commandline -a ' > /dev/null 2>&1 &;'"
end

function fish_greeting
set emotion (shuf -n 1 -e b d g p s t w y )
set say_list (cowsay -l | tail -n +2 | tr ' ' '\n'| grep -v pony|grep -v unipony|grep -v calvin)
set say_type (shuf -n 1 -z -e $say_list )
set say_what (fortune | fmt -80 -s )
# set do_type (shuf -n 1 -e cowsay cowthink)
# set rum_cmd $do_type -$emotion -f $say_type -W 80 "$say_what"
# eval $rum_cmd
# echo $rum_cmd
#   fortune -a | fmt -76 -s | $(shuf -n 1 -e cowsay cowthink) -$(shuf -n 1 -e b d g p s t w y) -f $(shuf -n 1 -e $(cowsay -l | tail -n +2)) -n


# echo "####################### Welcome fishing $USER #######################"
echo "$say_what" | cowthink -$emotion -f $say_type -W 76
echo "---- TODO: --------------------------------------------------------"
cat -n ~/.config/fish/todo.lst
end

set __fish_git_prompt_show_informative_status 1
set -gx PATH /usr/local/sbin ~/anaconda/bin $PATH

. ~/.bash_aliases
/bin/bash /etc/profile
/bin/bash ~/.profile
